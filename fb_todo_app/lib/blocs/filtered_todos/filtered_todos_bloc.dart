// ignore_for_file: no_leading_underscores_for_local_identifiers
import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import '../../models/todo_model.dart';
import '../blocs.dart';

part 'filtered_todos_event.dart';
part 'filtered_todos_state.dart';

class FilteredTodosBloc extends Bloc<FilteredTodosEvent, FilteredTodosState> {
  late StreamSubscription todoFilterSubcription;
  late StreamSubscription todoSearchSubcription;
  late StreamSubscription todoListSubcription;

  final List<Todo> initialTodos;
  final TodoFilterBloc todoFilterBloc;
  final TodoSearchBloc todoSearchBloc;
  final TodoListBloc todoListBloc;
  FilteredTodosBloc({
    required this.initialTodos,
    required this.todoFilterBloc,
    required this.todoSearchBloc,
    required this.todoListBloc,
  }) : super(FilteredTodosState(filteredTodos: initialTodos)) {
    todoFilterSubcription =
        todoFilterBloc.stream.listen((TodoFilterState todoFilterState) {
          setFilteredTodos();
        });

    todoSearchSubcription =
        todoSearchBloc.stream.listen((TodoSearchState todoSearchState) {
          setFilteredTodos();
        });

    todoListSubcription =
        todoListBloc.stream.listen((TodoListState todoListState) {
          setFilteredTodos();
        });
    on<CalculateFilteredTodosEvent>((event, emit) {
      emit(state.copyWith(filteredTodos: event.filteredTodos));
    });
  }

  void setFilteredTodos() {
    List<Todo> _filteredTodos;

    switch (todoFilterBloc.state.filter) {
      case Filter.active:
        _filteredTodos = todoListBloc.state.todos
            .where((Todo todo) => !todo.completed)
            .toList();
        break;
      case Filter.completed:
      default:
        _filteredTodos = todoListBloc.state.todos
            .where((Todo todo) => todo.completed)
            .toList();
        break;
      // case Filter.all:
      
      //   _filteredTodos = todoListBloc.state.todos;
      //   break;
    }
    if (todoSearchBloc.state.searchTerm.isNotEmpty) {
      _filteredTodos = _filteredTodos
          .where((Todo todo) => todo.desc
              .toLowerCase()
              .contains(todoSearchBloc.state.searchTerm))
          .toList();
    }

    add(CalculateFilteredTodosEvent(filteredTodos: _filteredTodos));
  }

  @override
  Future<void> close() {
    todoFilterSubcription.cancel();
    todoSearchSubcription.cancel();
    todoListSubcription.cancel();
    return super.close();
  }
}
