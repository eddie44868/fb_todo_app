part of 'todo_list_bloc.dart';

class TodoListState extends Equatable {
  final List<Todo> todos;
    const TodoListState({
    required this.todos
  });

  factory TodoListState.initial() {
    return TodoListState(todos: [
      Todo(desc: "Clean the room", id: "1", addTime: ''),
      Todo(desc: "Wash the dish", id: "2", addTime: ''),
      Todo(desc: "Do homework", id: "3", addTime: ''),
    ]);
  }

  @override
  List<Object?> get props => [todos];

  @override
  String toString() {
    return 'TodoListState{todos=$todos}';
  }
  TodoListState copyWith({
    List<Todo>? todos    
  }) {
    return TodoListState(
          todos: todos ?? this.todos
      );
    }
  }
