// ignore_for_file: public_member_api_docs, sort_constructors_first
part of 'todo_list_bloc.dart';

sealed class TodoListEvent extends Equatable {
  const TodoListEvent();

  @override
  List<Object> get props => [];
}

class AddTodoEvent extends TodoListEvent {
  final String todoDesc;
  final String addTime;
  const AddTodoEvent({
    required this.todoDesc,
    required this.addTime,
  });

@override
  String toString() {
    return 'AddTodoEvent{todoDesc=$todoDesc, addTime=$addTime}';
  }

  @override
  List<Object> get props => [todoDesc, addTime];
}

class EditTodoEvent extends TodoListEvent {
  final String id;
  final String todoDesc;
  final String addTime;
  const EditTodoEvent({
    required this.id,
    required this.todoDesc,
    required this.addTime
  });

@override
  String toString() {
    return 'EditTodoEvent{id=$id, todoDesc=$todoDesc, addTime=$addTime}';
  }

  @override
  List<Object> get props => [id, todoDesc, addTime];
}

class ToggleTodoEvent extends TodoListEvent {
  final String id;
  const ToggleTodoEvent({
    required this.id,
  });

  @override
  String toString() {
    return 'ToggleTodoEvent{id=$id}';
  }

  @override
  List<Object> get props => [id];
}

class RemoveTodoEvent extends TodoListEvent {
  final Todo todo;
  const RemoveTodoEvent({
    required this.todo,
  });

  @override
  String toString() {
    return 'RemoveTodoEvent{todo=$todo}';
  }

  @override
  List<Object> get props => [todo];
}
