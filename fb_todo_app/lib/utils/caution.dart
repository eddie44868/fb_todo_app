import 'package:flutter/material.dart';

void caution (BuildContext context, String word) {
  ScaffoldMessenger.of(context).showSnackBar(
    SnackBar(
      behavior: SnackBarBehavior.floating,
      backgroundColor: Colors.transparent,
      elevation: 0,
      content: Container(
        padding: const EdgeInsets.all(10),
        height: 90,
        width: 150,
        decoration: BoxDecoration(
          color: Colors.red,
          borderRadius: BorderRadius.circular(20)
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            const Text('Ooopps!!!', style: TextStyle(fontSize: 15),),
            const SizedBox(height: 5),
            Text(word, style: const TextStyle(fontSize: 15), textAlign: TextAlign.center, maxLines: 2,)
          ],
        ),
      )
    )
  );
}