import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fb_todo_app/blocs/auth/auth_bloc.dart';
import 'package:fb_todo_app/blocs/signin/signin_cubit.dart';
import 'package:fb_todo_app/page/home_page.dart';
import 'package:fb_todo_app/page/signin_page.dart';
import 'package:fb_todo_app/page/signup_page.dart';
import 'package:fb_todo_app/page/splash_page.dart';
import 'package:fb_todo_app/page/splash_page2.dart';
import 'package:fb_todo_app/repositories/auth_repository.dart';
import 'package:fb_todo_app/repositories/profile_repository.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'blocs/profile/profile_cubit.dart';
import 'blocs/signup/signup_cubit.dart';
import 'blocs/blocs.dart';
import 'firebase_options.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<AuthRepository>(
            create: (context) => AuthRepository(
                firebaseFirestore: FirebaseFirestore.instance,
                firebaseAuth: FirebaseAuth.instance)),
        RepositoryProvider<ProfileRepository>(
            create: (context) => ProfileRepository(
                firebaseFirestore: FirebaseFirestore.instance,)),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<AuthBloc>(
              create: (context) =>
                  AuthBloc(authRepository: context.read<AuthRepository>())),
          BlocProvider<SigninCubit>(
              create: (context) =>
                  SigninCubit(authRepository: context.read<AuthRepository>())),
          BlocProvider<SignupCubit>(
              create: (context) => SignupCubit(
                    authRepository: context.read<AuthRepository>(),
                  )),
          BlocProvider<TodoFilterBloc>(create: (context) => TodoFilterBloc()),
          BlocProvider<TodoSearchBloc>(create: (context) => TodoSearchBloc()),
          BlocProvider<TodoListBloc>(create: (context) => TodoListBloc()),
          BlocProvider<ActiveTodoCountBloc>(
              create: (context) => ActiveTodoCountBloc(
                  initialActiveTodoCount:
                      context.read<TodoListBloc>().state.todos.length,
                  todoListBloc: BlocProvider.of<TodoListBloc>(context))),
          BlocProvider<FilteredTodosBloc>(
              create: (context) => FilteredTodosBloc(
                  initialTodos: context.read<TodoListBloc>().state.todos,
                  todoFilterBloc: BlocProvider.of<TodoFilterBloc>(context),
                  todoSearchBloc: BlocProvider.of<TodoSearchBloc>(context),
                  todoListBloc: BlocProvider.of<TodoListBloc>(context))),
          BlocProvider<ProfileCubit>(
              create: (context) => ProfileCubit(
                    profileRepository: context.read<ProfileRepository>(),
                  )),
        ],
        child: MaterialApp(
          debugShowCheckedModeBanner: false,
          title: 'TODO App',
          theme: ThemeData(
            colorScheme: ColorScheme.fromSeed(seedColor: Colors.blue),
            useMaterial3: true,
          ),
          home: const SplashPage(),
          routes: {
            SignupPage.routeName: (context) => const SignupPage(),
            SigninPage.routeName: (context) => const SigninPage(),
            HomePage.routeName: (context) => const HomePage(),
            Splash2Page.routeName: (context) => const Splash2Page(),
          },
        ),
      ),
    );
  }
}
