import 'package:fb_todo_app/blocs/signin/signin_cubit.dart';
import 'package:fb_todo_app/page/signup_page.dart';
import 'package:fb_todo_app/repositories/auth_repository.dart';
import 'package:fb_todo_app/utils/caution.dart';
import 'package:fb_todo_app/utils/error_dialog.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SigninPage extends StatefulWidget {
  static const String routeName = '/Signin';
  const SigninPage({super.key});

  @override
  State<SigninPage> createState() => _SigninPageState();
}

class _SigninPageState extends State<SigninPage> {
  bool? checkBoxValue = false;
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  AutovalidateMode _autovalidateMode = AutovalidateMode.disabled;
  TextEditingController emailController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  bool _passwordVisible = false;

  void _submit() {
    setState(() {
      _autovalidateMode = AutovalidateMode.always;
    });

    final form = _formKey.currentState;

    if (form == null || !form.validate()) return;

    form.save();

    context.read<SigninCubit>().signin(email: emailController.text, password: passwordController.text);
  }

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: BlocConsumer<SigninCubit, SigninState>(//
        listener: (context, state) {
          if (state.signinStatus == SigninStatus.error) {
            errorDialog(context, state.customError);
          }
        },
        builder: (context, state) {
          return Scaffold(
              body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: SafeArea(
              child: Padding(
                padding: const EdgeInsets.only(left: 15, right: 15, bottom: 15),
                child: Form(
                  key: _formKey,
                  autovalidateMode: _autovalidateMode,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(right: 120),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: const Icon(
                                  Icons.arrow_back_ios,
                                  size: 25,
                                )),
                            const Text(
                              "Sign In",
                              style: TextStyle(
                                  fontSize: 25, fontWeight: FontWeight.w500),
                            ),
                          ],
                        ),
                      ),
                      Center(
                          child: Image.asset(
                        "assets/images/wfo.png",
                        height: 300,
                        width: 300,
                      )),
                      const Text(
                        "Welcome Back!",
                        style:
                            TextStyle(fontSize: 30, fontWeight: FontWeight.w500),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      const Text(
                        "Fill your details or continue with\nsocial media",
                        style:
                            TextStyle(fontSize: 15, fontWeight: FontWeight.w400),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      const Text(
                        "Email Address",
                        style:
                            TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 50,
                        width: 330,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.grey.shade200),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: TextFormField(
                            controller: emailController,
                            keyboardType: TextInputType.emailAddress,
                            autocorrect: false,
                            cursorColor: Colors.blue.shade900,
                            decoration:
                                const InputDecoration(border: InputBorder.none),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      const Text(
                        "Password",
                        style:
                            TextStyle(fontSize: 15, fontWeight: FontWeight.w300),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Container(
                        height: 50,
                        width: 330,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            color: Colors.grey.shade200),
                        child: Padding(
                          padding: const EdgeInsets.only(left: 10, right: 10),
                          child: TextFormField(
                            controller: passwordController,
                            obscureText: !_passwordVisible,
                            cursorColor: Colors.blue.shade900,
                            decoration:
                                InputDecoration(
                                  border: InputBorder.none,
                                  suffixIcon: IconButton(//
                                  icon: Icon(
                                    _passwordVisible
                                        ? Icons.visibility
                                        : Icons.visibility_off,
                                    color: Colors.black45,
                                  ),
                                  onPressed: () {
                                    setState(() {
                                      _passwordVisible = !_passwordVisible;
                                    });
                                  },
                                ),
                                ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Row(
                            children: [
                              Checkbox(
                                  value: checkBoxValue,
                                  onChanged: (bool? newValue) {
                                    setState(() {
                                      checkBoxValue = newValue;
                                    });
                                  }),
                              const Text(
                                "Remember Me",
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.w300),
                              ),
                            ],
                          ),
                          TextButton(
                              onPressed: () {},
                              child: const Text(
                                'Forget Password',
                                style: TextStyle(
                                    fontSize: 15, fontWeight: FontWeight.w300),
                              ))
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Center(
                        child: SizedBox(
                          height: 50,
                          width: widthScreen,
                          child: ElevatedButton(
                            onPressed: () {
                              if (emailController.text == '' || emailController.text.trim().isEmpty) {
                                caution(context, 'Enter a valid email');
                              } 
                              else if (passwordController.text == '' || passwordController.text.trim().isEmpty || passwordController.text.trim().length < 6) {
                                caution(context, 'Password must be at least 6 characters long');
                              } 
                              else {
                                state.signinStatus == SigninStatus.submitting
                                  ? null
                                  : _submit();
                              }
                            },
                            style: ElevatedButton.styleFrom(
                                backgroundColor: Colors.blue.shade300,
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(15))),
                            child: Center(
                              child: Text(
                                state.signinStatus == SigninStatus.submitting
                                    ? 'Loading...'
                                    : 'Login',
                                style: const TextStyle(
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold,
                                    color: Colors.white),
                              ),
                            ),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 20, right: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text(
                              "Don't have an account?",
                              style: TextStyle(
                                  fontSize: 15,
                                  fontWeight: FontWeight.w300,
                                  color: Colors.black),
                            ),
                            const SizedBox(
                              width: 15,
                            ),
                            TextButton(
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, SignupPage.routeName);
                                },
                                child: const Text(
                                  "Register",
                                  style: TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w300,
                                      color: Colors.blue),
                                ))
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Container(
                            height: 1,
                            width: 10,
                            decoration: const BoxDecoration(color: Colors.black),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          const Text(
                            "Or Continue with",
                            style: TextStyle(
                                fontSize: 15,
                                fontWeight: FontWeight.w300,
                                color: Colors.black),
                          ),
                          const SizedBox(
                            width: 15,
                          ),
                          Container(
                            height: 1,
                            width: 10,
                            decoration: const BoxDecoration(color: Colors.black),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: 15,
                      ),
                      Center(
                        child: InkWell(
                          onTap: () { //
                            AuthService().signInwithGoogle();
                          },
                          child: Container(
                            height: 50,
                            width: 50,
                            decoration: BoxDecoration(
                              border: Border.all(),
                                borderRadius: BorderRadius.circular(10)),
                                
                            child: Image.asset('assets/images/google.png'),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ));
        },
      ),
    );
  }
}
