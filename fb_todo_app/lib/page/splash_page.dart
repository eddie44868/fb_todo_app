import 'package:fb_todo_app/blocs/auth/auth_bloc.dart';
import 'package:fb_todo_app/page/home_page.dart';
import 'package:fb_todo_app/page/splash_page2.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class SplashPage extends StatelessWidget {
  static const String routeName = '/';
  const SplashPage({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocConsumer<AuthBloc, AuthState>(
      listener: (context, state) {
        if (state.authStatus == AuthStatus.unauthenticated) {//
          Navigator.pushNamed(context, Splash2Page.routeName);
        } else if (state.authStatus == AuthStatus.authenticated) {
          Navigator.pushNamed(context, HomePage.routeName);
        }
      },
      builder: (context, state) {
        return const Scaffold(
          body: CircularProgressIndicator(),
        );
      },
    );
  }
}
