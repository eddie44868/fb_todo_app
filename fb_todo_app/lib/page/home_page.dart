import 'package:fb_todo_app/page/todos_page/create_todo.dart';
import 'package:fb_todo_app/page/todos_page/search_and_filter.dart';
import 'package:fb_todo_app/page/todos_page/show_todos.dart';
import 'package:fb_todo_app/page/todos_page/todos_header.dart';
import 'package:flutter/material.dart';
import 'todos_page/todos_option.dart';

class HomePage extends StatefulWidget {
  static const String routeName = '/home';
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: GestureDetector(
        onTap: () {
          FocusManager.instance.primaryFocus?.unfocus();
        },
        child: Scaffold(
          endDrawerEnableOpenDragGesture: false,
          appBar: const TodoHeader(),
          // drawer: const DrawerScreen(),
          body: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: SafeArea(
              child: Column(
                children: [
                  SearchAndFilterTodo(),
                  const TodoOption(),
                  const SizedBox(height: 20,),
                  const ShowTodos(),
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50)),
              elevation: 5,
              backgroundColor: Colors.blue[300],
              onPressed: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => CreateTodo(
                            addTime: '',
                            id: '',
                            todoDesc: '',
                          )),
                );
              },
              child: const Icon(Icons.add, color: Colors.white)),
        ),
      ),
    );
  }
}
