import 'package:fb_todo_app/page/signin_page.dart';
import 'package:flutter/material.dart';

class Splash2Page extends StatelessWidget {
  static const String routeName = '/Splash2';
  const Splash2Page({super.key});

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: Scaffold(
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.only(top: 10, left: 15, right: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                const Text("TODO.",
                  style: TextStyle(fontSize: 25, fontWeight: FontWeight.w500),
                ),
                const SizedBox(height: 45,),
                Image.asset("assets/images/partner.png"),
                const SizedBox(height: 30,),
                const Text("Everything you\nneed in one app",
                  style: TextStyle(fontSize: 30, fontWeight: FontWeight.w500,),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 25,),
                const Text("With TODO app,\neverything is much easier",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300,),
                  textAlign: TextAlign.center,
                ),
                const SizedBox(height: 35,),
                SizedBox(
                  height: 50,
                  width: widthScreen,
                  child: ElevatedButton(
                    onPressed: () {
                     Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => const SigninPage()),
                      ); 
                    },
                    style: ElevatedButton.styleFrom(
                      backgroundColor: Colors.blue.shade300,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(15)
                      )
                    ),
                    child: const Center(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Let's Get Started",
                            style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: Colors.white),
                          ),
                          SizedBox(width: 15,),
                          Icon(Icons.arrow_forward, size: 30, color: Colors.white,)
                        ],
                      ),
                    ),
                  ),
                ),
                const SizedBox(height: 25,),
                Container(
                  height: 6,
                  width: 30,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(15),
                    color: Colors.blue.shade200
                  ),
                )
              ],
            ),
          ),
        )
      ),
    );
  }
}