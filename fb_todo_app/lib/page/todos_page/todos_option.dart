import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../blocs/blocs.dart';
import '../../models/todo_model.dart';

class TodoOption extends StatefulWidget {
  const TodoOption({super.key});

  @override
  State<TodoOption> createState() => _TodoOptionState();
}

class _TodoOptionState extends State<TodoOption> {
  String todoOption = 'Active';
  final List <String> _option = ['Active', ' Completed'];

  void onTapped(Filter filter) {
    context.read<TodoFilterBloc>().add(ChangeFilterEvent(newFilter: filter));
  }

  @override
  Widget build(BuildContext context) {
    final double widthScreen = MediaQuery.of(context).size.width;
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 15, right: 15,),
          child: Container(
            height: 50,
            width: widthScreen,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              border: Border.all()
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 15),
              child: DropdownButtonFormField<String>(
                decoration: const InputDecoration.collapsed(
                  hintText: ''
                ),
                items: _option.map((String value) => DropdownMenuItem<String>(value: value, child: Text(value),)).toList(),
                value: todoOption,
                onChanged: (value) {
                  setState(() {
                    todoOption = value!;
                  });
                  if (todoOption == 'Active') {
                    onTapped(Filter.active);
                  } else {
                    onTapped(Filter.completed);
                  }
                },
              ),
            ),
          ),
        ),
        // const SizedBox(height: 15,)
      ],
    );
  }
}