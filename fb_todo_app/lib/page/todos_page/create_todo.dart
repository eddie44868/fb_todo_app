// ignore_for_file: public_member_api_docs, sort_constructors_first, must_be_immutable
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:fb_todo_app/utils/caution.dart';
import '../../blocs/todo_list/todo_list_bloc.dart';

class CreateTodo extends StatefulWidget {
  static const String routeName = '/create';
  String id;
  String todoDesc;
  String addTime;
  CreateTodo({
    Key? key,
    required this.id,
    required this.todoDesc,
    required this.addTime,
  }) : super(key: key);

  @override
  State<CreateTodo> createState() => _CreateTodoState();
}

class _CreateTodoState extends State<CreateTodo> {
  final TextEditingController newTodoController = TextEditingController();
  final formKey = GlobalKey<FormState>();
  DateTime date = DateTime(2023, 09, 25);
  String showDate = '';
  bool? checkBoxValue = false;
  bool isSwitched = false;

  void _submit() {
    context
        .read<TodoListBloc>()
        .add(AddTodoEvent(todoDesc: newTodoController.text, addTime: showDate));
    newTodoController.clear();
  }

  void _submitAgain() {
    context.read<TodoListBloc>().add(EditTodoEvent(
        id: widget.id, todoDesc: newTodoController.text, addTime: showDate));
  }

  @override
  void initState() {
    super.initState();
    formKey;
    newTodoController.text = widget.todoDesc;
    showDate = widget.addTime;
  }

  @override
  void dispose() {
    newTodoController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        FocusManager.instance.primaryFocus?.unfocus();
      },
      child: BlocListener<TodoListBloc, TodoListState>(
        listener: (context, state) {
          if (widget.id.isEmpty) {
            var snackBar = const SnackBar(
              content: Text('Todo Created Successfully.'),
              duration: Duration(milliseconds: 1500),
            );
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          } else {
            var snackBar = const SnackBar(
              content: Text('Todo Edited Successfully.'),
              duration: Duration(milliseconds: 1500),
            );
            Navigator.pop(context);
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        },
        child: Scaffold(
          appBar: AppBar(
            title: const Text('Create Todo'),
          ),
          body: SafeArea(
            child: Padding(
              padding: const EdgeInsets.only(left: 15, right: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text(
                    'What to do?',
                    style: TextStyle(fontSize: 17),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  Container(
                    height: 50,
                    width: 330,
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                        color: Colors.grey.shade200),
                    child: Padding(
                      padding: const EdgeInsets.only(left: 10, right: 10),
                      child: TextFormField(
                        key: formKey,
                        controller: newTodoController,
                        keyboardType: TextInputType.text,
                        autocorrect: false,
                        cursorColor: Colors.blue.shade900,
                        decoration:
                            const InputDecoration(border: InputBorder.none),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 20,
                  ),
                  Row(
                    children: [
                      Switch(
                        value: isSwitched,
                        onChanged: (value) {
                          setState(() {
                            isSwitched = value;
                          });
                        },
                        activeTrackColor: Colors.blueAccent,
                        activeColor: Colors.white,
                      ),
                      const SizedBox(
                        width: 10,
                      ),
                      const Text(
                        "Using Date",
                        style: TextStyle(
                            fontSize: 15, fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  const Text(
                    'Date',
                    style: TextStyle(fontSize: 17),
                  ),
                  const SizedBox(
                    height: 10,
                  ),
                  (isSwitched == true)
                      ? InkWell(
                          onTap: () async {
                            DateTime? newDate = await showDatePicker(
                                builder: (context, child) {
                                  return Theme(
                                    data: ThemeData().copyWith(
                                        colorScheme: ColorScheme.light(
                                            primary: Colors.blue.shade400)),
                                    child: child!,
                                  );
                                },
                                context: context,
                                initialDate: date,
                                firstDate: DateTime(1950),
                                lastDate: DateTime(2100));
                            if (newDate == null) return;
                            setState(() {
                              date = newDate;
                              showDate = date.toString().substring(0, 10);
                            });
                          },
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Container(
                                  height: 50,
                                  width: 280,
                                  decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(15),
                                      color: Colors.grey.shade200),
                                  child: Center(
                                      child: Text(
                                    (showDate == '2023-09-25' || showDate == '')
                                        ? 'Pick a Date'
                                        : showDate,
                                    style: const TextStyle(
                                        fontSize: 15,
                                        fontWeight: FontWeight.w400,
                                        color: Colors.grey),
                                  ))),
                              const Icon(
                                Icons.date_range,
                                size: 40,
                              ),
                            ],
                          ),
                        )
                      : Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                                height: 50,
                                width: 280,
                                decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(15),
                                    color: Colors.grey.shade200),
                                child: Center(
                                    child: Text(
                                  (showDate == '')
                                      ? 'Pick a Date'
                                      : showDate, //
                                  style: const TextStyle(
                                      fontSize: 15,
                                      fontWeight: FontWeight.w400,
                                      color: Colors.grey),
                                ))),
                            const Icon(
                              Icons.date_range,
                              size: 40,
                            ),
                          ],
                        ),
                  const SizedBox(
                    height: 10,
                  ),
                ],
              ),
            ),
          ),
          floatingActionButton: FloatingActionButton(
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50)),
              elevation: 5,
              backgroundColor: Colors.blue[300],
              onPressed: () {
                if (showDate == '2023-09-25') {
                  showDate = '';
                }
                if (newTodoController.text == '' ||
                  newTodoController.text.trim().isEmpty) {
                  caution(context, 'Enter what to do!');
                } else {
                  if (widget.id == '') {
                    _submit();
                  } else {
                    _submitAgain();
                  }
                }
              },
              child: const Icon(Icons.check, color: Colors.white)),
        ),
      ),
    );
  }
}
