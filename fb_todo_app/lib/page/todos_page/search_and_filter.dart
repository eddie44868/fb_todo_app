import 'package:fb_todo_app/blocs/blocs.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../blocs/todo_search/todo_search_bloc.dart';
import '../../utils/debouce.dart';

class SearchAndFilterTodo extends StatelessWidget {
  SearchAndFilterTodo({super.key});
  final debounce = Debounce(milliseconds: 1000);

  @override
  Widget build(BuildContext context) {
    // final double widthScreen = MediaQuery.of(context).size.width;
    return Column(
      children: [
        Center(
          child: Padding(
            padding: const EdgeInsets.only(left: 15, right: 15),
            child: TextField(
              decoration: InputDecoration(
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(15.0),
                  ),
                  filled: true,
                  contentPadding: const EdgeInsets.symmetric(horizontal: 15),
                  prefixIcon: const Icon(Icons.search)),
              onChanged: (String? newSearchTerm) {
                if (newSearchTerm != null) {
                    context.read<TodoSearchBloc>().add(SetSearchTermEvent(searchTerm: newSearchTerm));
                }
              },
            ),
          ),
        ),
        const SizedBox(height: 10),
        // Row(
        //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //   children: [
        //     filterButton(context, Filter.all),
        //     filterButton(context, Filter.active),
        //     filterButton(context, Filter.completed),
        //   ],
        // )
      ],
    );
  }

  // Widget filterButton(BuildContext context, Filter filter) {
  //   return TextButton(
  //       onPressed: () {
  //         context.read<TodoFilterBloc>().add(ChangeFilterEvent(newFilter: filter));
  //       },
  //       child: Text(
  //         filter == Filter.all
  //             ? 'All'
  //             : filter == Filter.active
  //                 ? 'Active'
  //                 : 'Completed',
  //         style: TextStyle(fontSize: 18, color: textColor(context, filter)),
  //       ));
  // }

  // Color textColor(BuildContext context, Filter filter) {
  //   final currentFilter = context.watch<TodoFilterBloc>().state.filter;
  //   return currentFilter == filter ? Colors.blue : Colors.grey;
  // }
}
