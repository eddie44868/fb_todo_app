// ignore_for_file: no_leading_underscores_for_local_identifiers
import 'package:fb_todo_app/blocs/blocs.dart';
import 'package:fb_todo_app/page/todos_page/create_todo.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../models/todo_model.dart';

class ShowTodos extends StatelessWidget {
  const ShowTodos({super.key});

  @override
  Widget build(BuildContext context) {
    final todos = context.watch<FilteredTodosBloc>().state.filteredTodos;
    return Column(
      children: [
        (todos.isEmpty)
            ? const Center(
              child: Text('No Data'))
            : ListView.builder(
                primary: false,
                shrinkWrap: true,
                itemBuilder: (BuildContext context, int index) {
                  return ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: Dismissible(
                      key: ValueKey(todos[index].id),
                      background: showBackground(0),
                      secondaryBackground: showBackground(1),
                      onDismissed: (_) {
                        context
                            .read<TodoListBloc>()
                            .add(RemoveTodoEvent(todo: todos[index]));
                      },
                      confirmDismiss: (_) {
                        return showDialog(
                            context: context,
                            barrierDismissible: false,
                            builder: (context) {
                              return AlertDialog(
                                title: const Text('Are you sure?'),
                                content: const Text('Do you want to delete?'),
                                actions: [
                                  TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, false),
                                      child: const Text('NO')),
                                  TextButton(
                                      onPressed: () =>
                                          Navigator.pop(context, true),
                                      child: const Text('YES')),
                                ],
                              );
                            });
                      },
                      child: Column(
                        children: [
                          const SizedBox(
                            height: 5,
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 15, right: 15),
                            child: TodoItem(todo: todos[index]),
                          ),
                          const SizedBox(
                            height: 5,
                          )
                        ],
                      ),
                    ),
                  );
                },
                itemCount: todos.length),
      ],
    );
  }
}

Widget showBackground(int direction) {
  return Container(
    margin: const EdgeInsets.all(4),
    padding: const EdgeInsets.symmetric(horizontal: 10),
    color: Colors.red,
    alignment: direction == 0 ? Alignment.centerLeft : Alignment.centerRight,
    child: const Icon(
      Icons.delete,
      size: 30,
      color: Colors.white,
    ),
  );
}

class TodoItem extends StatefulWidget {
  final Todo todo;
  const TodoItem({
    Key? key,
    required this.todo,
  }) : super(key: key);

  @override
  State<TodoItem> createState() => _TodoItemState();
}

class _TodoItemState extends State<TodoItem> {
  late final TextEditingController textController;

  @override
  void initState() {
    super.initState();
    textController = TextEditingController();
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListTile(
      shape: RoundedRectangleBorder(
        side: const BorderSide(width: 1),
        borderRadius: BorderRadius.circular(20),
      ),
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => CreateTodo(
                    addTime: widget.todo.addTime,
                    id: widget.todo.id,
                    todoDesc: widget.todo.desc,
                  )),
        );
      },
      leading: Checkbox(
          value: widget.todo.completed,
          onChanged: (bool? checked) {
            context
                .read<TodoListBloc>()
                .add(ToggleTodoEvent(id: widget.todo.id));
          }),
      title: Row(
        children: [
          const SizedBox(
            width: 30,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(widget.todo.desc),
              Row(
                children: [
                  const Text('Date : '),
                  Text(widget.todo.addTime == ''
                      ? '  -  '
                      : widget.todo.addTime),
                ],
              ),
            ],
          ),
        ],
      ),
    );
  }
}
