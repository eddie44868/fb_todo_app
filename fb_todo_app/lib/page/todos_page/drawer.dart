// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:fb_todo_app/blocs/todo_filter/todo_filter_bloc.dart';
import 'package:fb_todo_app/models/todo_model.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../blocs/auth/auth_bloc.dart';
import '../../blocs/profile/profile_cubit.dart';
import '../../utils/error_dialog.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({super.key});

  @override
  State<DrawerScreen> createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  int _selected = 0;
  final userId = FirebaseAuth.instance.currentUser!.uid;

  @override
  void initState() {
    super.initState();
    _getProfile();
  }

  void _getProfile() {
    final String uid = context.read<AuthBloc>().state.user!.uid;
    context.read<ProfileCubit>().getProfile(uid: uid);
  }

  void onTapped(int index, Filter filter) {
    setState(() {
      _selected = index;
      context.read<TodoFilterBloc>().add(ChangeFilterEvent(newFilter: filter));
    });
  }

  @override
  Widget build(BuildContext ctx) {
    return SizedBox(
      width: 230,
      child: BlocConsumer<ProfileCubit, ProfileState>(
        listener: (context, state) {
          if (state.profileStatus == ProfileStatus.error) {
            errorDialog(context, state.error);
          }
        },
        builder: (context, state) {
          return Drawer(
            child: ListView(
              padding: EdgeInsets.zero,
              children: [
                DrawerHeader(
                  decoration: BoxDecoration(
                    color: Colors.blue[300],
                  ),
                  child: Column(
                    children: [
                      Container(
                        height: 60,
                        width: 60,
                        decoration: const BoxDecoration(
                            color: Colors.white, shape: BoxShape.circle),
                        child: const Icon(
                          Icons.person_2_outlined,
                          size: 50,
                        ),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                      Text('Name : ${state.user.name}',
                        style: const TextStyle(color: Colors.white),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Text(state.user.email,
                        style: const TextStyle(color: Colors.white, fontSize: 12),
                      ),
                      const SizedBox(
                        height: 10,
                      ),
                    ],
                  ),
                ),
                // ListTile(
                //   leading: const Icon(Icons.ballot_outlined),
                //   title: const Text(
                //     'All',
                //     textAlign: TextAlign.center,
                //   ),
                //   selected: _selected == 0,
                //   onTap: () {
                //     onTapped(0, Filter.all);
                //     Navigator.pop(context);
                //   },
                // ),
                const Divider(),
                ListTile(
                  leading: const Icon(Icons.ballot_outlined),
                  title: const Text(
                    'Active',
                    textAlign: TextAlign.center,
                  ),
                  selected: _selected == 1,
                  onTap: () {
                    onTapped(1, Filter.active);
                    Navigator.pop(context);
                  },
                ),
                const Divider(),
                ListTile(
                  leading: const Icon(Icons.ballot_outlined),
                  title: const Text(
                    'Completed',
                    textAlign: TextAlign.center,
                  ),
                  selected: _selected == 2,
                  onTap: () {
                    onTapped(2, Filter.completed);
                    Navigator.pop(context);
                  },
                ),
                const Divider(),
              ],
            ),
          );
        },
      ),
    );
  }
}

class DrawerListTile extends StatelessWidget {
  final String title;
  final VoidCallback onTilePressed;
  final bool selectedIndex;

  const DrawerListTile({
    Key? key,
    required this.title,
    required this.onTilePressed,
    required this.selectedIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: onTilePressed,
      leading: const Icon(Icons.menu_open),
      title: Text(
        title,
        style: const TextStyle(fontSize: 16),
      ),
      selected: selectedIndex == selectedIndex,
    );
  }
}
