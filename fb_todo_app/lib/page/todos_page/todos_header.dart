// ignore_for_file: library_private_types_in_public_api
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import '../../blocs/auth/auth_bloc.dart';

class TodoHeader extends StatefulWidget implements PreferredSizeWidget {
  const TodoHeader({super.key})
      : preferredSize = const Size.fromHeight(kToolbarHeight);

  @override
  final Size preferredSize;

  @override
  _TodoHeaderState createState() => _TodoHeaderState();
}

class _TodoHeaderState extends State<TodoHeader> {

  showAlertDialog(BuildContext context) {
    Widget cancelbutton = TextButton(
      onPressed: () {
        Navigator.pop(context);
      },
      child: const Text('Cancel'));

    Widget continuebutton = TextButton(
        onPressed: () {
          context.read<AuthBloc>().add(SignoutRequestedEvent());
        },
        child: const Text('Continue'));

    AlertDialog alert = AlertDialog(
      title: const Text('ALERT!!!', textAlign: TextAlign.center,),
      content: const Text('Want to exit the app?', textAlign: TextAlign.center,),
      actions: [
        cancelbutton,
        continuebutton
      ],
      actionsAlignment: MainAxisAlignment.spaceBetween,
    );

    showDialog(context: context, builder: (BuildContext context) {
      return alert;
    });
  }

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: const Text('TODO.'),
      centerTitle: true,
      automaticallyImplyLeading: false,
      // leading: IconButton(
      //     onPressed: () {
      //       Scaffold.of(context).openDrawer();
      //     },
      //     icon: const Icon(Icons.menu)),
      actions: [
        IconButton(
            onPressed: () {
              showAlertDialog(context);
            },
            icon: const Icon(Icons.exit_to_app)),
      ],
    );
  }
}
