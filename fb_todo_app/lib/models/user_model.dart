// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';

class User extends Equatable {
  final String id;
  final String name;
  final String email;
  const User({
    required this.id,
    required this.name,
    required this.email,
  });

  factory User.fromDoc(DocumentSnapshot userDoc) {
    final userData = userDoc.data() as Map<String, dynamic>?;

    return User(id: userDoc.id, name: userData!['name'], email: userData['email']);
  }

  factory User.initialUser() {
    return const User(id: '', name: '', email: '');
  }

  @override
  List<Object?> get props => [id, name, email];

  @override
  String toString() {
      return 'UserModel{id=$id, name=$name, email=$email}';
    }
}
