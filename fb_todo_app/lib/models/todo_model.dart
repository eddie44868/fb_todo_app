// ignore_for_file: public_member_api_docs, sort_constructors_first
import 'package:equatable/equatable.dart';
import 'package:uuid/uuid.dart';

enum Filter {
  // all,
  active,
  completed,
}

Uuid uuid = const Uuid();

class Todo extends Equatable {
  final String id;
  final String desc;
  final String addTime;
  final bool completed;
  // final DateTime date;
  Todo({
    String? id,
    required this.desc,
    required this.addTime,
    this.completed = false,
  }) : id = id ?? uuid.v4();

  @override
  List<Object?> get props => [id, desc, addTime, completed];

  @override
  String toString() {
      return 'Todo{id=$id, desc=$desc, addTime=$addTime, completed=$completed}';
    }
}
